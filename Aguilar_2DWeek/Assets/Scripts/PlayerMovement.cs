﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    Rigidbody2D rB2D;

    public TextMeshProUGUI coincountText;
    public float runSpeed;
    public float jumpForce;
    public SpriteRenderer spriterenderer;
    public Animator animator;
    public GameObject coinPickUp;
    public AudioSource pickupcoin;
    

    private int coinScore;
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        coinScore = 0;
        SetCoinCountText();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))

            

        {
            int levelMask = LayerMask.GetMask("Level");
            
            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
                animator.SetBool("IsJumping", true);
            }
            else
            {
                animator.SetBool("IsJumping", false);
            }
        }
    }
    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
        {
            spriterenderer.flipX = false;
        }
        else
        if (rB2D.velocity.x < 0)
        {
            spriterenderer.flipX = true;
        }


        if (Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("IsRunning", true);
            animator.SetBool("IsJumping", false);
        }
        else
        {
            animator.SetBool("IsRunning", false);
            
        }
    }

    void SetCoinCountText()
    {
        coincountText.text = coinScore.ToString();
    }


    void AudioPlaying(AudioSource audioSource)
    {
        audioSource.Play();
    }


    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
           
            coinScore = coinScore + 1;
            print("collected coin" + coinScore);
            AudioPlaying(pickupcoin);
            SetCoinCountText();
        }
        if (other.gameObject.CompareTag("Reset"))
        {
            SceneManager.LoadScene(0);
        }


    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Spider"))
        {


            SceneManager.LoadScene(0);

        }
    }
}
